
      // Lista de articulos
      let mListArticles = [];

      // Verifica si el modal agrega o edita elementos
      let isAdd = true;

      // Tabla de articulos
      let mTblArticles = document.getElementById('tbl_articles');

      function editArticle(index) {

        let element = mListArticles.findIndex(index);




      }


      /**
       * Guarda nuevos elementos en la lista actual
       */
      function addArticle () {

        // Elementos que obtenemos del formulario
        let name = document.getElementById('name').value;
        let cant = document.getElementById('cant').value;
        let units = document.getElementById('units').value;
        let exp_date = document.getElementById('exp_date').value;
        let type = document.getElementById('type').value;
        let lot = document.getElementById('lot').value;
        let invima = document.getElementById('invima').value;

        // Creamos el objeto artíulo
        const article = {
          name,
          cant,
          units,
          exp_date,
          type,
          lot,
          invima
        }

        // Agreamos el objeto artículo a la lista de articulos
        mListArticles.push(article);

        // Elimina todas las filas existentes
        deleteAllRowsTable();

        // Inserta todos los articulos existentes
        insertAllArticlesToTable();

        //console.log(mListArticles);

        return false;
      }

      /**
       * Elimina todos los elementos de la tabla
       */
      function deleteAllRowsTable () {

        // Cantidad de elementos que tiene la tabla
        let rowCount = mTblArticles.rows.length;

        // Para evitar errores de eliminacion
        let tableHeaderRowCount = 1;

        // Se eliminan las filas de la tabla
        for (let i = tableHeaderRowCount; i < rowCount; i++) {
          mTblArticles.deleteRow(tableHeaderRowCount);
        }

      }

      /**
       * Elimina todos los elementos de la tabla y la lsita de objetos
       */
      function deleteTable () {
        // Inicializamos nuestra lista de articulos en vacia
        mListArticles = [];
        // Eliminar todos los elementos de la tabla
        deleteAllRowsTable();
      }

      /**
       * Inserta todos los articulos existentes en la tabla
       */
      function insertAllArticlesToTable () {

        try {

          // Obtenemos el body de la tabla
          let tbody = document.getElementById("data");

          // Obtenemos el total de columnas que va a tener la tabla
          let totalColumns = Object.keys(mListArticles[0]).length;

          // Fila y columna
          let tr, td;

          // Se recorren todos los ariculos
          for (let i = 0; i < mListArticles.length; i++) {

            // Objeto articulo
            const article = mListArticles[i];

            // Se crea una nueva fila
            tr = tbody.insertRow(tbody.rows.length);

            // Celda para elemento identificado de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = i + 1;

            // Celda para elemento nombre de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = article.name;

            // Celda para elemento nombre de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = article.cant;

            // Celda para elemento unidades de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = article.units;

            // Celda para elemento fecha de expiracion de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = article.exp_date;

            // Celda para elemento tipo de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = article.type;

            // Celda para elemento lote de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = article.lot;

            // Celda para elemento invima de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = article.invima;

            // Celda para elemento acciones de articulo
            td = tr.insertCell(tr.cells.length);
            td.innerHTML = '<button type="button" class="btn btn-warning" onclick="editArticle()">Editar elemento</button>';

          }

        } catch (err) {
          console.log(err)
          return false;
        }
      }
