

// Lista de elementos ingresados
let mListUser = []

/**
 * Agrega una nuevo elemento a la lista
 */
function add() {

    // Nombre del usuario
    let name = document.getElementById('name').value;

    // Edad del usuario
    let age = document.getElementById('age').value;

    // Objeto usuario con los elementos ingresados
    const user = {
        name,
        age
    };

    // Se agrega el nuevo objeto creado a la lista de usuarios
    mListUser.push(user);

    console.log('USUARIO:', user);

    return false;

}

/**
 * Cuenta el número de elementos de la lista actual
 */
function countElements() {
    /**
     * Promesa para evaluar la cantidad de elementos de una lista
     */
    new Promise((resolve, rejec) => {
        // Cantidad de elementos de la laista
        let cont = mListUser.length;

        // Verifica si existen elementos
        if (cont > 0) {
            // Retorna la cantidad de elementos
            resolve(cont);
        } else {
            // Mensaje de no hay elementos
            rejec('No hay elementos para mostrar')
        }
    })
    .then(res => {
        // imprime la cantidad de elementos ingresados
        console.log('Elementos:', mListUser);
        console.log('Cantidad de elementos ingresados:', res);
    })
    .catch(err => {
        // imprime el error capuirado
        console.error(err);
    });
}

/**
 * Calcula el promedio de la edad de los usuarios ingresados
 */
function promAges() {
    /**
     * Promesa para evaluar la cantidad de elementos de una lista
     */
    new Promise((resolve, rejec) => {
        
        // Verifica si hay elementos en la lista
        if (mListUser.length > 0) {

            // Cantidad de elementos de la laista
            let sumAge = 0;

            // Se recorren todos los usuarios disponibles
            for (const k in mListUser) {
                if (mListUser.hasOwnProperty(k)) {
                    const user = mListUser[k];
                    // Suma de edades
                    sumAge += user.age;
                }
            }

            // Promedio de edades
            let avg = sumAge / mListUser.length;
            
            // Retorna promedio de edades
            resolve(avg);
        } else {
            // Mensaje de no hay elementos
            rejec('No hay elementos para mostrar')
        }
        
        
    })
    .then(res => {
        // imprime la cantidad de elementos ingresados
        console.log('Promedio de edad de los usuarios ingresados', res);
    })
    .catch(err => {
        // imprime el error capuirado
        console.error(err);
    });
}